from gym.envs.registration import register

#################_CUSTOM_ENVS_############################

register(
    id='HybridSimple_all_right_pick_and_reachEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_AllRightJointsPickAndReach',
    # entry_point='giantml.contrib.envs.robot_img_env:SomeTest',
    max_episode_steps=100
)

register(
    id='HybridSimple_fixed_one_body_jointEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleFixedOneBodyJoint',
    max_episode_steps=100
)

register(
    id='HybridSimple_one_body_jointEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleOneBodyJoint',
    max_episode_steps=100
)

register(
    id='HybridSimple_fixed_two_arm_jointsEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleFixedTwoArmJoints',
    max_episode_steps=100
)

register(
    id='HybridSimple_two_arm_jointsEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleTwoArmJoints',
    max_episode_steps=100
)

register(
    id='HybridSimple_fixed_all_right_jointsEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_FixedAllRightJoints',
    max_episode_steps=100
)

register(
    id='HybridSimple_all_right_jointsEnv-v1',
    entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_AllRightJoints',
    max_episode_steps=100
)

#########################################################

register(
    id='HybridHammerEnv-v1',
    entry_point='environments.metaworld.constructors:HybridHammerEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridDcEnv-v1',
    entry_point='environments.metaworld.constructors:HybridDrawerCloseEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridPwEnv-v1',
    entry_point='environments.metaworld.constructors:HybridReachPushWallEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridBcEnv-v1',
    entry_point='environments.metaworld.constructors:HybridBoxCloseEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridDoorEnv-v1',
    entry_point='environments.metaworld.constructors:HybridDoorOpenEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridPusEnv-v1',
    entry_point='environments.metaworld.constructors:HybridPegUnplugSideEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridWoEnv-v1',
    entry_point='environments.metaworld.constructors:HybridWindowOpenEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridPickplaceEnv-v1',
    entry_point='environments.metaworld.constructors:HybridPickPlaceEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)

register(
    id='HybridAntEnv-v1',
    entry_point='environments.locomotion.constructors:HybridAntEnvV1',
    max_episode_steps=1000,
    reward_threshold=2500.0
)

register(
    id='HybridAntncEnv-v1',
    entry_point='environments.locomotion.constructors:HybridAntncEnvV1',
    max_episode_steps=1000,
    reward_threshold=12500.0
)

