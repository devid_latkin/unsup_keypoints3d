import torch
import gym
import torchvision

from ..common.bc.bc_base_algo import UnsupBcAlgo
from ..common.utils import preprocess_obs


class KeypointBcAlgo(UnsupBcAlgo):

    # def flatten_multiview_offset(self, offset):
    #     batch, frame_stack, num_cameras = offset.shape
    #     # if self.latent_stack:
    #     #     offset = offset.view(batch * frame_stack, num_cameras)
    #     # else:
    #     #     offset = offset[:, 0]
    #
    #     offset = offset.view(batch * frame_stack, num_cameras)
    #
    #     return offset
    #
    # def flatten_multiview_images(self, images):
    #     """
    #     :param images: [batch, frame_stack, num_cameras, h, w ,c]
    #     :return: processed_images: []
    #     """
    #     batch, frame_stack, num_cameras, h, w, c = images.shape
    #     # if self.latent_stack:
    #     #     processed_images = images.permute(0, 1, 2, 5, 3, 4)
    #     #     processed_images = processed_images.reshape(batch * frame_stack, num_cameras * c, h, w)
    #     # else:
    #     #     processed_images = images.permute(0, 2, 1, 5, 3, 4)
    #     #     processed_images = processed_images.reshape(batch, num_cameras * frame_stack * c, h, w)
    #
    #     processed_images = images.permute(0, 1, 2, 5, 3, 4)
    #     processed_images = processed_images.reshape(batch * frame_stack, num_cameras * c, h, w)
    #
    #     return processed_images
    #
    # def process_multiview_obs(self, obs):
    #     # if isinstance(self.observation_space, gym.spaces.Dict):
    #     #     obs = obs.copy()
    #     #     obs['images'] = self.flatten_multiview_images(obs['images'])
    #     #     if self.offset_crop:
    #     #         obs['u_shift'] = self.flatten_multiview_offset(obs['u_shift'])
    #     #         obs['v_shift'] = self.flatten_multiview_offset(obs['v_shift'])
    #     #     if self.first_frame is not None:
    #     #         obs['first_frame'] = self.flatten_multiview_images(obs['first_frame'])
    #     # elif isinstance(self.observation_space, gym.spaces.Tuple):
    #     #     raise NotImplementedError
    #     # else:
    #     #     obs = self.flatten_multiview_images(obs)
    #
    #     obs['u_shift'] = self.flatten_multiview_offset(obs['u_shift'])
    #     obs['v_shift'] = self.flatten_multiview_offset(obs['v_shift'])
    #     obs['first_frame'] = self.flatten_multiview_images(obs['first_frame'])
    #
    #     return obs

    def train_unsup(self):
        obs_data = self.obs_buffer.sample(self.batch_size, env=None)
        # obs_data = self.obs_buffer[0]

        obs_data = self.policy.process_multiview_obs(obs_data)
        # obs_data = self.process_multiview_obs(obs_data)


        obs_data = preprocess_obs(obs_data, self.observation_space)
        images = obs_data['images']
        u_shift = obs_data['u_shift'] if self.policy.offset_crop else None
        v_shift = obs_data['v_shift'] if self.policy.offset_crop else None
        first_frame = obs_data['first_frame'] if self.policy.first_frame is not None else None

        # print("v_shift:", v_shift)
        keypoints, unsup_loss_dict, _ = self.policy.unsup_net.encode(images, rsample=True, u_shift=u_shift, v_shift=v_shift)
        # print("keypoints:", keypoints)

        images_hat = self.policy.unsup_net.decode(keypoints, u_shift=u_shift, v_shift=v_shift, first_frame=first_frame)
        unsup_loss_dict['ae_loss'] = self.policy.ae_criteria(images_hat, images)

        unsup_loss_dict = {k: v.mean() for k, v in unsup_loss_dict.items()}
        unsup_loss = sum([v * self.unsup_coef_dict[k] for k, v in unsup_loss_dict.items()])

        self.policy.unsup_optimizer.zero_grad()
        unsup_loss.backward()

        if torch.isnan(unsup_loss):
            print("value is nan")
            exit(1)

        unsup_grad_norm = torch.nn.utils.clip_grad_norm_(
            self.policy.unsup_net.parameters(),
            1.5
        ).item()
        # unsup_grad_norm = 100
        self.policy.unsup_optimizer.step()

        return unsup_loss_dict, unsup_grad_norm

    def save_visualization(self, path, batch_size=16):
        obs_data = self.obs_buffer.sample(batch_size, env=None)
        vis_tensor = self.policy.visualize(obs_data)
        torchvision.utils.save_image(vis_tensor, path, 1, padding=5)

