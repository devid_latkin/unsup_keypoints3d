from experiments import algo_registry, get_args


if __name__ == '__main__':
    args = get_args()

    print("Used algo: ", args.algo)

    experiment = algo_registry[args.algo](args)


    # print("experiment: ", experiment)
    # print("self.policy_class: ", experiment.policy_class)
    # print("experiment.algo_kwargs: ", experiment.algo_kwargs)
    # exit(1)


    experiment.train()
