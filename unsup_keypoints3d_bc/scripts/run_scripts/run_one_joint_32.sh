export PYTHONPATH=$PYTHONPATH:/scratch/devid.latkin/projects/research/reach_box/gym:/scratch/devid.latkin/projects/research/reach_box/model:/scratch/devid.latkin/projects/research/reach_box/giantml:/scratch/devid.latkin/projects/research/reach_box/giant_utils/python &&
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/devid.latkin/.mujoco/mujoco200/bin &&
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/nvidia-460 &&
cd ../.. &&
python3 train.py --algo ppokeypoint -t simple_one_body_joint -v 1 -e 0008 -m 3d -j --frame_stack 2 --latent_stack --total_timesteps 6000000 --unsup_steps 400 --num_envs 16 --mean_depth 1.5 --decode_attention --offset_crop --decode_first_frame --num_keypoint 32 --seed 123 -u --separation_coef 0.005 --learning_rate=0.0001