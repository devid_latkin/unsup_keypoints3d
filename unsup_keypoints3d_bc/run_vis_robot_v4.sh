export PYTHONPATH=$PYTHONPATH:/scratch/devid.latkin/projects/research/reach_box/gym:/scratch/devid.latkin/projects/research/reach_box/model:/scratch/devid.latkin/projects/research/reach_box/giantml:/scratch/devid.latkin/projects/research/reach_box/giant_utils/python &&
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/devid.latkin/.mujoco/mujoco200/bin &&
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/nvidia-460 &&
python3 visualize.py --algo ppokeypoint -t robot -v 1 -e 0001 -m 3d -j --total_timesteps 6000000 --offset_crop --decode_first_frame --num_keypoint 3 --decode_attention --seed 200 -u
