import numpy as np
import cv2

# path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_1_target_as_input.avi"
# path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_3_target_as_input.avi"
# path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_9_target_as_input.avi"
# path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_32_target_as_input.avi"
# output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_concat_target_as_input.avi"

# path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_fixed_1.avi"
# path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_fixed_3.avi"
# path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_fixed_9.avi"
# path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_fixed_32.avi"
# output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/one_fixed_concat_target_as_input.avi"


# path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_fixed_1.avi"
# path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_fixed_3.avi"
# path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_fixed_9.avi"
# path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_fixed_32.avi"
# output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_fixed_concat.avi"


# path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_1_target_as_input.avi"
# path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_3_target_as_input.avi"
# path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_9_target_as_input.avi"
# path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_32_target_as_input.avi"
# output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/two_concat_target_as_input.avi"


# path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_fixed_1.avi"
# path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_fixed_3.avi"
# path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_fixed_9.avi"
# path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_fixed_32.avi"
# output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_fixed_concat.avi"


path_to_demo_video_1 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_1.avi"
path_to_demo_video_3 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_3.avi"
path_to_demo_video_9 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_9.avi"
path_to_demo_video_32 = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_32.avi"
output_path = "/home/david/projects/giant_ai/giant_research/unsup_keypoints3d/images/all_concat.avi"


init_writer = False
out = None

def main():
    global init_writer
    cap_1 = cv2.VideoCapture(path_to_demo_video_1)
    cap_3 = cv2.VideoCapture(path_to_demo_video_3)
    cap_9 = cv2.VideoCapture(path_to_demo_video_9)
    cap_32 = cv2.VideoCapture(path_to_demo_video_32)

    if (cap_1.isOpened() == False):
        print("Error opening video stream or file")

    if (cap_3.isOpened() == False):
        print("Error opening video stream or file")

    if (cap_9.isOpened() == False):
        print("Error opening video stream or file")

    if (cap_32.isOpened() == False):
        print("Error opening video stream or file")

    while (cap_1.isOpened()) and (cap_3.isOpened()) and (cap_9.isOpened()) and (cap_32.isOpened()):
        # Capture frame-by-frame
        ret_1, frame_1 = cap_1.read()
        ret_3, frame_3 = cap_3.read()
        ret_9, frame_9 = cap_9.read()
        ret_32, frame_32 = cap_32.read()

        if ret_1 == True and ret_3 == True and ret_9 == True and ret_32 == True:

            h, w, c = frame_1.shape

            points_only_1 = frame_1[:, w // 3: 2*w // 3, :]
            points_only_3 = frame_3[:, w // 3: 2*w // 3, :]
            points_only_9 = frame_9[:, w // 3: 2*w // 3, :]
            points_only_32 = frame_32[:, w // 3: 2*w // 3, :]

            cv2.imshow('points_only_1', points_only_1)
            cv2.imshow('points_only_3', points_only_3)
            cv2.imshow('points_only_9', points_only_9)
            cv2.imshow('points_only_32', points_only_32)

            concat_img = np.array(np.concatenate([points_only_1, points_only_3, points_only_9, points_only_32], axis=1), dtype=np.uint8)
            cv2.imshow('concat_img', concat_img)

            if not init_writer:
                fourcc = cv2.VideoWriter_fourcc(*'XVID')
                out = cv2.VideoWriter(output_path, fourcc, 10, (concat_img.shape[1], concat_img.shape[0]))
                init_writer = True

            if init_writer:
                out.write(concat_img)

            # Press Q on keyboard to  exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break

    out.release()

    # When everything done, release the video capture object
    cap_1.release()
    cap_3.release()
    cap_9.release()
    cap_32.release()

    # Closes all the frames
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()