import os
import cv2
import torch
from torch import nn
import gym
import numpy as np
from gym.envs.registration import register


visualize_hammer = False
visualize_robot = True


def register_custom_env():
    register(
        id='Test-v1',
        # entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleOneBodyJoint',
        entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_AllRightJointsPickAndReach',
        # entry_point='giantml.contrib.envs.robot_img_env:SomeTest',
        max_episode_steps=200
    )


register(
    id='HybridHammerEnvNew-v1',
    entry_point='environments.metaworld.constructors:HybridHammerEnvV1',
    max_episode_steps=200,
    kwargs={'mode': 'train'}
)


class KeypointProjTorch(nn.Module):
    def __init__(self, images, Vs, P):
        super().__init__()
        self.Vs = torch.tensor(Vs)
        self.register_buffer("V_invs", torch.inverse(self.Vs))
        self.num_cameras = 3
        self.num_keypoints = 3
        self.epsilon = 1e-8
        self.batch_size = 1
        self.images = images

        self.P = torch.FloatTensor(P)

        P_inv = torch.FloatTensor([[1.0 / self.P[0, 0], 0, 0, 0],
                                   [0, 1.0 / self.P[1, 1], 0, 0],
                                   [0, 0, -1.0 / self.P[2, 2], 0],
                                   [0, 0, 0, 1.0]])

        self.register_buffer("P_inv", P_inv)

    def xyz_world_to_cam(self, xyz_world):
        batch_size, num_points, _ = xyz_world.shape
        xyzw_world = torch.ones((batch_size, num_points, 4), dtype=xyz_world.dtype, device=xyz_world.device)
        xyzw_world[:, :, :3] = xyz_world
        xyzw_cam = torch.einsum('bjn,cmn->bcjm', xyzw_world, self.Vs)
        xyzw_cam = xyzw_cam / (xyzw_cam[:, :, :, 3:4] + self.epsilon)  # (batch, num_cam, num_points, 4)
        return xyzw_cam

    def xyz_world_to_xy_normalized(self, xyz_world):
        batch_size, num_points, _ = xyz_world.shape
        xyzw_cam = self.xyz_world_to_cam(xyz_world)
        xyzw_cam = xyzw_cam.reshape(batch_size * self.num_cameras * num_points, 4)
        xy_normalized = self.project(xyzw_cam).reshape(batch_size, self.num_cameras, num_points, 2)
        return xy_normalized

    def project(self, xyzw):
        xyzw = torch.matmul(xyzw, self.P.T)
        xyzw = xyzw / (xyzw[:, 2:3] + self.epsilon)
        xy_normalized = xyzw[:, :2]
        return xy_normalized

    def unproject(self, xy_normalized, z):
        xy = torch.cat([-xy_normalized * z, z, torch.ones_like(z)], 1)
        xyzw = torch.matmul(xy, self.P_inv.T)
        xyzw = xyzw / (xyzw[:, 3:4] + self.epsilon)
        return xyzw

    def multiview_loss(self, xyz_world):
        """
        :param xyz_world: Tensor (batch, num_cam, num_points, 3)
        :return: multiview_loss: (batch, 1)
        """
        batch_size, _, num_points, _ = xyz_world.shape

        xyz_world_target = xyz_world[:, :, None].expand(-1, -1, self.num_cameras, -1, -1).detach()
        xyz_world = xyz_world[:, None].expand(-1, self.num_cameras, -1, -1, -1)
        multiview_loss = nn.functional.mse_loss(xyz_world, xyz_world_target)

        return multiview_loss

    def reproject_points(self):
        # xy_normalized = torch.tensor([[0.1, 0.1]])
        # z = torch.tensor([[-0.47]])
        #
        # xy = torch.cat([-xy_normalized * z, z, torch.ones_like(z)], 1)

        xy_normalized = torch.tensor([[0., 0.],
                                      [0.2, -0.2],
                                      [-0.2, 0.2],
                                      [-0.01, 0.01],
                                      [0.01, 0.01],
                                      [0.1, -0.1],
                                      [-0.01, -0.01],
                                      [0.01, 0.01],
                                      [0.1, -0.1],])
        # z = torch.tensor([[-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709],
        #                   [-0.4709]
        #                   ])

        z = torch.tensor([[-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5],
                          [-1.5]
                          ])

        xy = torch.cat([-xy_normalized * z, z, torch.ones_like(z)], 1)

        print("xy: ", xy)

        xyzw_cam = self.unproject(xy_normalized, z).reshape(self.batch_size, self.num_cameras, self.num_keypoints, 4)
        xyzw_world = torch.einsum('cnm,bckm->bckn', self.V_invs, xyzw_cam)
        xyz_world = (xyzw_world / (xyzw_world[:, :, :, 3:4] + self.epsilon))[:, :, :, :3]

        xy_world, z_world = torch.split(xyz_world, [2, 1], 3)
        xyz_world = torch.cat([xy_world, z_world], -1)

        print("xyzw_world: ", xyz_world)
        print("xyzw_world: ", xyz_world.shape)

        # xyz_world = xyz_world.reshape(self.batch_size, self.num_cameras*self.num_keypoints, 3)
        # xyz_world = xyz_world.reshape(self.batch_size, self.num_cameras*self.num_keypoints, 3)

        xy_normalized_repr = self.xyz_world_to_xy_normalized(xyz_world[:, 0, :, :])

        print("xy_normalized: ", xy_normalized)
        print("xy_normalized_repr: ", xy_normalized_repr)

        # xy_repr = torch.cat([-xy_normalized_repr * z_world, z_world, torch.ones_like(z_world)], 1)
        # print("xy_repr: ", xy_repr)
        # xy_repr = xy_repr.numpy()
        # print(xy_normalized_repr_np[0][0][0][1])
        print(xy_normalized_repr.shape)

        b = 0

        # c1 = 0
        n_cameras = 3

        # multiview loss
        m_loss = self.multiview_loss(xyz_world)
        print("m_loss: ", m_loss)

        for j in range(n_cameras):
            c1 = j
            for i, (x, y) in enumerate(xy_normalized_repr[b, c1]):
                if i == 0:
                    color = (0, 0, 255)
                elif i == 1:
                    color = (0, 255, 0)
                else:
                    color = (255, 0, 0)
                x = x / 2 + 0.5
                y = y / 2 + 0.5
                x = int(x * self.images[0].shape[1])
                y = int((1 - y) * self.images[c1].shape[0])
                print("x, y: ", (x, y))
                # if scale[b, i] > att_threshold:
                cv2.circle(self.images[c1], (x, y), 5, color, -1)

        cv2.imshow("img_0", self.images[0])
        cv2.imshow("img_1", self.images[1])
        cv2.imshow("img_2", self.images[2])

        cv2.waitKey(0)


def show_images(obs, basename):
    image = obs['images']
    cv2.imshow(basename+"_0", image[0])
    cv2.imshow(basename+"_1", image[1])
    cv2.imshow(basename+"_2", image[2])


def check_reprojections_2(images, Vs, P):
    print("starting reproj new")
    t = KeypointProjTorch(images, Vs, P)
    t.reproject_points()

    print("end reproj")


def main():

    if visualize_hammer:
        env_t = gym.make('HybridHammerEnvNew-v1')
        print("Init done")
        # reset
        reset_obs_t = env_t.reset()
        show_images(reset_obs_t, "t_reset")

        # action
        a_t = np.zeros([3], dtype=float)
        step_obs_t, step_reward_t, step_done_t, step_info_t = env_t.step(a_t)

        images = step_obs_t['images']
        show_images(step_obs_t, "t_reset")

        # check projections correctness
        Vs_t = env_t.env.view_matrices
        P_t = env_t.env.projection_matrix
        check_reprojections_2(images, Vs_t, P_t)

    if visualize_robot:
        register_custom_env()
        env_c = gym.make('Test-v1')

        # class Aaa():
        #     def __init__(self):
        #         self.env = gym.make('Test-v1')
        # env_c = Aaa()


        print("Init done")
        # reset
        reset_obs_c = env_c.reset()
        show_images(reset_obs_c, "c_reset")
        a_c = np.zeros([env_c.action_space.shape[0]], dtype=float)
        step_obs_c, step_reward_c, step_done_c, step_info_c = env_c.step(a_c)
        print("step_obs_c: ", step_obs_c)
        show_images(step_obs_c, "c_reset")
        images = step_obs_c['images']

        Vs_c = env_c.env.view_matrices
        P_c = env_c.env.projection_matrix

        # Vs_c = env_c.view_matrices
        # P_c = env_c.projection_matrix

        # print("reward_range", env_c._env.reward_range)
        print("reward_range", env_c.env.reward_range)

        check_reprojections_2(images, Vs_c, P_c)

    cv2.waitKey(0)



if __name__ == '__main__':
    main()