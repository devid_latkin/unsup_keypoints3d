import gym

from stable_baselines3 import PPO

# env = gym.make("CartPole-v1")
# env = gym.make("MountainCar-v0")
# env = gym.make("Pendulum-v0")
# env = gym.make("HalfCheetah-v2")
env = gym.make("Ant-v2")

model = PPO("MlpPolicy", env, verbose=1)
model.learn(total_timesteps=10000)

obs = env.reset()
for i in range(1000):
    action, _states = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    print(done)
    print(reward)
    if done:
      obs = env.reset()

env.close()