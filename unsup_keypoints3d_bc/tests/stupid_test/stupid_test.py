import numpy as np
import os


class Env(object):
    def __init__(self):
        self.var = 10


class Class_1(object):
    def __init__(self):
        self.env = Env()


class Class_2():
    def __init__(self):
        # super().__init__()
        self.cl_1 = Class_1()

def main():
    cl2 = Class_2()

    print(cl2.cl_1.env.var)

    pass


def generator():
    for i in range(10):
        yield i

def generator_test():

    for i in range(10):
        print(generator().__next__())




if __name__ == '__main__':
    generator_test()


    # main()