import torch
from torch import nn
import numpy as np
from icecream import ic

class SimpleNet(nn.Module):
    def __init__(self):
        super().__init__()

        self.in_features_dim = 10
        self.build_model(in_features_dim=self.in_features_dim)

    def build_model(self, in_features_dim):
        self.model = nn.Sequential(
            nn.Linear(in_features_dim, 1),
            # nn.ReLU(),
            # nn.Linear(64, 1),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.model(input)


def main():

    model = SimpleNet()
    optimizer = torch.optim.Adam(model.parameters(), weight_decay=0.9)


    loss = torch.nn.functional.mse_loss

    n_steps = 1
    target_vals = torch.tensor(np.array([np.random.random() for _ in range(n_steps)], dtype=np.float32))
    for i in range(n_steps):
        random_vec = np.array(np.random.random(10), dtype=np.float32)
        ic(random_vec)

        input_torch = torch.tensor(random_vec)
        ic(input_torch)

        pred_val = model.forward(input_torch)
        target_val = target_vals[i]
        # ic(pred_val.data)
        # ic(pred_val.grad)
        # for param in model.parameters():
        #     # ic(param.data[0][0])
        #     # ic(param.data)
        #     if param.grad is not None:
        #         # ic(param.grad[10][0])
        #         ic(param.grad)
        #     else:
        #         ic(param.grad)
        #     break

        # optimizer.zero_grad()
        loss(pred_val, target_val).backward()
        param = model.parameters().__next__()
        ic(param.grad)
        grad_norm = torch.nn.utils.clip_grad_norm_(
            model.parameters(),
            0.01
        ).item()
        ic(param.grad)
        ic(grad_norm)

        optimizer.step()


if __name__ == '__main__':
    main()