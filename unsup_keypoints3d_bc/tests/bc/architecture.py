import os
import cv2
import torch
from torch import nn
import gym
import numpy as np
from gym.envs.registration import register
from algorithms.common.models.keypoint_net import KeypointNet3d
from torchsummary import summary


# 1. need to define 'observation_space', 'crop_size', 'projection_matrix', and 'view_matrices'

def register_custom_env():
    register(
        id='Test-v1',
        # entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_SimpleOneBodyJoint',
        entry_point='giantml.contrib.envs.robot_img_env:RobotUniversalReachImgUnsupervised3D_AllRightJointsPickAndReach',
        # entry_point='giantml.contrib.envs.robot_img_env:SomeTest',
        max_episode_steps=200
    )


def main():
    register_custom_env()
    env_c = gym.make('Test-v1')

    Vs_c = env_c.view_matrices
    P_c = env_c.projection_matrix
    #
    # print("view_matrices: ", Vs_c)
    # print("projection_matrix: ", P_c)

    crop_size = 108

    camera_height = 128
    camera_width = 128
    frame_stack = 1
    num_cameras = 3
    num_keypoints = 9
    mean_depth = 1.0

    observation_space = gym.spaces.Dict({
        'images': gym.spaces.Box(low=0.0, high=1.0, shape=(frame_stack,
                                                           num_cameras,
                                                           camera_height,
                                                           camera_width,
                                                           3), dtype=np.uint8),
        'robot_joints': gym.spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32),
        'robot_joint_positions': gym.spaces.Box(low=-1.0, high=1.0, shape=(1, 3))
    })


    model = KeypointNet3d(observation_space=observation_space,
                          crop_size=crop_size,
                          projection_matrix=P_c,
                          view_matrices=Vs_c,
                          num_keypoints=num_keypoints,
                          mean_depth=mean_depth)

    # Try forward
    a_c = np.zeros([env_c.action_space.shape[0]], dtype=float)
    step_obs_c, step_reward_c, step_done_c, step_info_c = env_c.step(a_c)
    images = step_obs_c['images']


    def to_torch_repr(image):
        # images:  torch.Size([2, 9, 108, 108]) =>  [frame stack, num_of_cams*num_of_channels, size, size]
        # w, h, c => c, w, h
        image = cv2.resize(image, (crop_size, crop_size))
        return np.swapaxes(np.swapaxes(image, 0, 2), 1, 2)

    print(images.shape)

    images_torch = np.array(np.concatenate([to_torch_repr(images[0]), to_torch_repr(images[1]), to_torch_repr(images[2])]), dtype=np.float32)
    images_torch = torch.unsqueeze(torch.from_numpy(images_torch), dim=0)

    print("images_torch: ", images_torch.shape)

    output = model.forward(images=images_torch)

    print(output)
    summary(model, images_torch.shape)







if __name__ == '__main__':
    main()

