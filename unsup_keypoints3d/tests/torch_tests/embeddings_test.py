from torch import nn
import torch
from icecream import ic

n, d, m = 3, 5, 7
embedding = nn.Embedding(n, d, max_norm=True)
W = torch.randn((m, d), requires_grad=True)
idx = torch.tensor([1, 2])

ic(embedding)
# ic(embedding.weight)