import torch
from torch import nn
import numpy as np
from icecream import ic

import matplotlib.pyplot as plt


def visualize_data(points, gt_labels, pred_labels):
    plt.scatter(points[:, 0], points[:, 1], c=gt_labels)
    plt.show()

# Move this into jupiter and structure data
def data_generation(n_points):
    outliers_prob = 0.02

    def get_val(x, y):
        return 0.1*x*x + 0.2*x*y + 0.3*y

    labels = []
    points = []
    for i in range(n_points):
        pt = [np.random.uniform(-1.0, 1.0), np.random.uniform(-1.0, 1.0)]
        val = get_val(*pt)
        if val >= 0:
            labels.append(1)
        else:
            labels.append(0)

        if np.random.random() < outliers_prob:
            labels[-1] = (labels[-1] + 1) % 2

        points.append(pt)
    # ic(labels)
    return np.array(points), np.array(labels)

def main():
    # Create data
    points, gt_labels = data_generation(n_points=100)

    # visualize data
    visualize_data(points, gt_labels, None)





if __name__ == '__main__':
    main()