from torch import nn
import torch
'''
Debug crush cases for encode
'''

class BaseCNN(nn.Module):
    def __init__(self, in_channels, out_channels=32, n_filters=32, groups=1):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.n_filters = n_filters
        self.groups = groups
        self.cnn = None
        self.build_model()

    def build_model(self):
        raise NotImplementedError

    def forward(self, x):
        return self.cnn(x)

    def infer_output_size(self, input_size):
        sample_input = torch.zeros(1, self.in_channels, input_size, input_size)
        with torch.no_grad():
            output = self.cnn(sample_input)

        return output.shape[-1]

class CustomeEncoder(BaseCNN):
    def build_model(self):
        self.cnn = nn.Sequential(
            nn.Conv2d(self.in_channels, self.n_filters, kernel_size=4, stride=2, groups=self.groups),
            nn.ReLU(),
            nn.Conv2d(self.n_filters, self.n_filters * 2, kernel_size=3, stride=2, groups=self.groups),
            nn.ReLU(),
            nn.Conv2d(self.n_filters * 2, self.out_channels, kernel_size=4, stride=2, groups=self.groups),
        )

class TestEncoder(object):
    def __init__(self):
        self.num_cameras = 3
        self.frame_stack = 1
        self.image_channels = 3
        self.n_filters = 16
        self.groups = 1



        self.encoder_cls = CustomeEncoder

        self.encoder_cls(self.num_cameras * self.frame_stack * self.image_channels,
                         self.n_filters * 4,
                         self.n_filters, groups=self.groups)

        self.encoder = self.encoder_cls(self.num_cameras * self.frame_stack * self.image_channels,
                                        self.n_filters * 4,
                                        self.n_filters, groups=self.groups)

        # self.heatmap_size = self.encoder.infer_output_size(self.image_size)
        # self.output_image_size = self.decoder.infer_output_size(self.heatmap_size)
        self.encoder = nn.Sequential(self.encoder, nn.ReLU())



def main():
    t_enc = TestEncoder()

    # take images


    # process inference:








if __name__ == '__main__':
    main()

